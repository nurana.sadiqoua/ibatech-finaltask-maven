package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageObjectPatternBasePage {

    public PageObjectPatternBasePage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

}
