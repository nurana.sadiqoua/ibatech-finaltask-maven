package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GlobalSQAPage extends PageObjectPatternBasePage{

    public GlobalSQAPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(css = "#menu-item-2822 > a")
    public WebElement TestersHubElement;

    @FindBy (css = "#menu-item-2823 > a")
    public WebElement DemoTestingSiteSubMenu;

    @FindBy (css = "#post-2715 > div:nth-child(3) > div > div > div.row.price_table_holder.col_4 > div:nth-child(1) > ul > li > a")
    public List<WebElement> FirstColumnElements;

    @FindBy (css = "#post-2715 > div:nth-child(3) > div > div > div.row.price_table_holder.col_4 > div:nth-child(2) > ul > li> a")
    public  List<WebElement> SecondColumnElements;

    @FindBy (css = "#post-2715 > div:nth-child(3) > div > div > div.row.price_table_holder.col_4 > div:nth-child(3) > ul > li > a")
    public List<WebElement> ThirdColumnElements;

    @FindBy (css = "#menu-item-2827 > a")
    public WebElement DatePickerSubMenuElement;

    @FindBy (css = ".demo-frame.lazyloaded")
    public WebElement DatePickerIFrame;

    @FindBy (css = "input#datepicker")
    public WebElement DatePickerInput;

    @FindBy (css = "[title='Next']")
    public WebElement DatePickerNextElement;

    @FindBy (css = "#ui-datepicker-div > table > tbody > tr:nth-child(5) > td:nth-child(5) > a")
    public WebElement DayToSelect;

    @FindBy (css = "#menu-item-2832 a")
    public WebElement ProgressBarSubMenu;

    @FindBy (css = ".demo-frame.lazyloaded")
    public WebElement DownloadIFrame;

    @FindBy (css = "button#downloadButton")
    public WebElement StartDownloadButton;

    @FindBy (css = "div#dialog")
    public  WebElement CompleteDisplayElement;
}
