import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.GlobalSQAPage;
import java.text.*;
import java.util.concurrent.TimeUnit;


public class FinalTaskTest {

    @Test
    public void verifyEqualityOfEachColumnElements() {
        //Arrange
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/driver/chromedriver.exe");
        WebDriver _driver = new ChromeDriver();
        _driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        _driver.manage().window().maximize();
        GlobalSQAPage globalSQAPage = new GlobalSQAPage(_driver);

        _driver.get("https://www.globalsqa.com/");
        int expectedNumberOfElementsInEachColumn = 6;

        //Act
        Actions actions = new Actions(_driver);
        actions.moveToElement(globalSQAPage.TestersHubElement).moveToElement(globalSQAPage.DemoTestingSiteSubMenu).click().perform();

        int actualNumberInFirstColumn = globalSQAPage.FirstColumnElements.size();
        int actualNumberInSecondColumn = globalSQAPage.SecondColumnElements.size();
        int actualNumberInThirdColumn = globalSQAPage.ThirdColumnElements.size();
        //Assert
        Assert.assertTrue(String.format("Number of items in first, second and third columns is %s, %s, %s, respectively." +
                        " At least one of them is not equal to %s",
                actualNumberInFirstColumn, actualNumberInSecondColumn, actualNumberInThirdColumn, expectedNumberOfElementsInEachColumn),
                expectedNumberOfElementsInEachColumn == actualNumberInFirstColumn &&
                        expectedNumberOfElementsInEachColumn == actualNumberInSecondColumn &&
                        expectedNumberOfElementsInEachColumn == actualNumberInThirdColumn);

        _driver.quit();
    }

    @Test
    public void verifyPickedDateFormat() {
        //Arrange
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/driver/chromedriver.exe");
        WebDriver _driver = new ChromeDriver();
        _driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        _driver.manage().window().maximize();
        GlobalSQAPage globalSQAPage = new GlobalSQAPage(_driver);

        _driver.get("https://www.globalsqa.com/");

        //Act
        Actions actions = new Actions(_driver);
        actions.moveToElement(globalSQAPage.TestersHubElement).moveToElement(globalSQAPage.DemoTestingSiteSubMenu)
                .moveToElement(globalSQAPage.DatePickerSubMenuElement).click().perform();

        _driver.switchTo().frame(globalSQAPage.DatePickerIFrame);
        globalSQAPage.DatePickerInput.click();
        globalSQAPage.DatePickerNextElement.click();
        globalSQAPage.DayToSelect.click();

        String strDate = globalSQAPage.DatePickerInput.getAttribute("value");

        //Assert
        Assert.assertTrue(validateJavaDate(strDate));

        _driver.quit();
    }

    public static boolean validateJavaDate(String strDate) {
        /* Check if date is 'null' */
        if (!strDate.trim().equals("")) {
            /*
             * Set preferred date format,
             * For example MM-dd-yyyy, MM.dd.yyyy,dd.MM.yyyy etc.*/
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            simpleDateFormat.setLenient(false);
            /* Create Date object
             * parse the string into date
             */
            try {
                simpleDateFormat.parse(strDate);
                System.out.println(strDate + " is valid date format");
            }
            /* Date format is invalid */ catch (ParseException e) {
                System.out.println(strDate + " is Invalid Date format");
                return false;
            }
            /* Return true if date format is valid */
        }
        return true;
    }

    @Test
    public void taskThree() {
        //Arrange
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/driver/chromedriver.exe");
        WebDriver _driver = new ChromeDriver();
        _driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        _driver.manage().window().maximize();
        GlobalSQAPage globalSQAPage = new GlobalSQAPage(_driver);
        String expectedMessage = "Complete!";

        _driver.get("https://www.globalsqa.com/");

        //Act
        Actions actions = new Actions(_driver);
        actions.moveToElement(globalSQAPage.TestersHubElement).moveToElement(globalSQAPage.DemoTestingSiteSubMenu)
                .moveToElement(globalSQAPage.ProgressBarSubMenu).click().perform();

        _driver.switchTo().frame(globalSQAPage.DownloadIFrame);
        globalSQAPage.StartDownloadButton.click();
        WebDriverWait wait = new WebDriverWait(_driver, 100);
        wait.until(ExpectedConditions.textToBe(By.cssSelector("div#dialog"), "Complete!"));

        String actualMessage = globalSQAPage.CompleteDisplayElement.getText();

        //Assert
        Assert.assertEquals("Download is not completed properly", expectedMessage, actualMessage);

        _driver.quit();
    }
}
